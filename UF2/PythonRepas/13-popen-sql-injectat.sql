# /user/bin/python3
#-*- coding: utf-8-*-
# @ Abril 2024
#programa.py ruta
#---------------------------------
import sys, argparse
from subprocess import Popen, PIPE
parser = argparse.ArgumentParser(description=\
        """Exemeple popen""")
args=parser.parse_args()
#---------------------------------
command = "psql -qtA -F ',' -h localhost -U postgres training"  #ordre exec
pipeData = Popen(command,shell=True,bufsize=0,universal_newlines=True,stdin=PIPE, stdout=PIPE, stderr=PIPE) # primera comanda
pipeData.stdin.write("select * from oficinas;\n\q\n") # sub comanda
for line in pipeData.stdout:
    print(line.decode("utf-8"),end="")
exit (0)
