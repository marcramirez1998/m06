import sys, socket, os, signal, argparse
from subprocess import Popen, PIPE

parser = argparse.ArgumentParser(description="Examen M06")
parser.add_argument("-d", "--debug")
parser.add_argument("-p", "--port", type=int, default=44444)
args = parser.parse_args()
HOST = ''
PORT = args.port
llistapeers = []

def llistar(signum, frame):
    print("Signal called:", signum)
    print(llistapeers)
    sys.exit(0)

def num(signum, frame):
    print("Signal called:", signum)
    print(len(llistapeers))
    sys.exit(0)

def tot(signum, frame):
    print("Signal called:", signum)
    print(llistapeers, len(llistapeers))
    sys.exit(0)

signal.signal(signal.SIGUSR1, llistar)
signal.signal(signal.SIGUSR2, num)
signal.signal(signal.SIGTERM, tot)

pid = os.fork()
if pid != 0:
    print("Server Started with Pid:", pid)
    sys.exit(0)

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST, PORT))
s.listen(1)

while True:
    conn, addr = s.accept()
    print("Connected by", addr)
    llistapeers.append(addr)
   
    while True:
        linea_recibida = conn.recv(1024)
        ComandoPers = linea_recibida.decode('utf-8')

        if not linea_recibida:
            break
        print(ComandoPers)

        if ComandoPers == "processos\n":
            command = "ps ax"
        elif ComandoPers == "ports\n":
            command = "ss -pta"
        elif ComandoPers == "whoareyou\n":
            command = "uname -a"
        else:
            command = "uname -a"

        PipeData = Popen(command, stdout=PIPE, shell=True)
        for line in PipeData.stdout:
            conn.send(line)
        conn.close()