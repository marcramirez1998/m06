#! /usr/bin/python3
#-*- coding: utf-8-*-
#
#@marcramirez1998 ASIX M06 Curs 2023-2024
# 22 client daytime
#------------------------------------------------------
import sys
import socket
import argparse
parser = argparse.ArgumentParser(description="""Client """)
parser.add_argument("-s","--server",type=str, default='')
parser.add_argument("-p","--port",type=int, default=50001)
args=parser.parse_args()
HOST = ''  # La dirección IP del servidor (en blanco significa todas las interfaces disponibles)
PORT = 50001  # El puerto en el que el servidor está escuchando

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # Se crea un socket TCP
s.connect((HOST, PORT))  # Se conecta al servidor en la dirección y puerto especificados

while True:  # Bucle infinito para recibir datos continuamente
    data = s.recv(1024)  # Se recibe un máximo de 1024 bytes de datos del servidor
    if not data:  # Si no hay más datos recibidos, se sale del bucle
        break
    print('Data:', repr(data))  # Se imprime la data recibida
s.close()  # Se cierra la conexión con el servidor
sys.exit(0)  # Se sale del script con un código de salida 0 (indicando que todo está bien)

