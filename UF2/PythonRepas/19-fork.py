#! /usr/bin/python3
#-*- coding: utf-8-*-
#
#@marcramirez1998 ASIX M06 Curs 2023-2024
# ejemplo fork
#-----------------------------
import sys
import os
import signal

def myusr1(signum, frame):
    print("Signal handler called with signal:", signum)
    print("Hola myusr1:")

def myusr2(signum, frame):
    print("Signal handler called with signal:", signum)
    print("Adiós myusr2:")

print("Hola, comienzo del programa principal")
print("PID padre:", os.getpid())
pid = os.fork()
if pid != 0:
    #os.wait()  
    print("Programa padre:", os.getpid(), pid)
    print("Llança el proces fill servidor")
    sys.exit(0)

    #programa fill  
#os.execv("/usr/bin/ls", ["/usr/bin/ls", "-la","/"])
#os.execl("/usr/bin/ls", "/usr/bin/ls", "-la","/")
#os.execlp("ls", "/ls", "-la","/")
#os.execvp("uname",["uname","-a"])
#os.execvpe("uname",["uname","-a"])
#os.execv("/bin/bash", ["/bin/bash","show.sh"])
os.execle("/bin/bash", "/bin/bash","show.sh",{"nom":"joan","edat":"25"})    
print("¡Hasta luego, Lucas!")
sys.exit(0)
