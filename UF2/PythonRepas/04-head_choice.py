#! /usr/bin/python3
#-*- coding: utf-8-*-
#
#@marcramirez1998 ASIX M06 Curs 2023-2024
#head [-n nlin] [-f file]
#  default=10 file o stdin
#-----------------------------
import sys, argparse
parser = argparse.ArgumentParser(description=\
    """Mostrar les N primereslinies """,\
        epilog="tot okey")
#-----------------------------
parser.add_argument("-n", "-nlin",type=int,\
    dest="nlin",help="número de línies a mostar",\
    metavar="numLineas",choices=[5,10,15],default=10)
parser.add_argument("-f", "-fit", type=str,\
    help="fitxer a processar (stdin)", metavar="file",\
    default="/dev/stdin",dest="fitxer")
args=parser.parse_args()
print(args)
#-----------------------------
MAX=args.nlin
fileIn=open(args.fitxer,"r")
counter=0
for line in fileIn:
    counter+=1
    print(line,end="")
    if counter==MAX: break
fileIn.close()
exit(0)
