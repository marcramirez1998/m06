#! /usr/bin/python3
#-*- coding: utf-8-*-
#
#@marcramirez1998 ASIX M06 Curs 2023-2024
# 24 server ss
#------------------------------------------------------
import sys, socket, os, signal, argparse  # Importa los módulos necesarios
from subprocess import Popen, PIPE  # Importa la clase Popen y PIPE del módulo subprocess

# Configuración de los argumentos de línea de comandos
parser = argparse.ArgumentParser(description="""ss server""")
parser.add_argument("-a", "--any", type=int, default=2022)  # Argumento para la dirección IP
parser.add_argument("-p", "--port", type=int, default=50001)  # Argumento para el puerto
args = parser.parse_args()  # Analiza los argumentos de la línea de comandos
llistaPeers = []  # Lista para almacenar las direcciones de los clientes
HOST = ''  # Dirección IP del servidor (en blanco significa todas las interfaces disponibles)
PORT = args.port  # Puerto en el que el servidor está escuchando
ANY = args.any  # Dirección IP a la que el servidor está vinculado

# Definición de funciones de manejo de señales
def mysigusr1(signum, frame):
    print("Signal handler called with signal:", signum)
    print(llistaPeers)
    sys.exit(0)

def mysigusr2(signum, frame):
    print("Signal handler called with signal:", signum)
    print(len(llistaPeers))
    sys.exit(0)

def mysigterm(signum, frame):
    print("Signal handler called with signal:", signum)
    print(llistaPeers, len(llistaPeers))
    sys.exit(0)

# Creación de un nuevo proceso hijo
pid = os.fork()
if pid != 0:  # Si el proceso es el padre
    print("Engegat el server ss:", pid)  # Muestra el ID del proceso hijo
    sys.exit(0)  # Termina el proceso padre

# Manejo de señales
signal.signal(signal.SIGUSR1, mysigusr1)
signal.signal(signal.SIGUSR2, mysigusr2)
signal.signal(signal.SIGTERM, mysigterm)

# Creación de un socket TCP
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)  # Permite reutilizar direcciones
s.bind((HOST, PORT))  # Vincula el socket a la dirección y puerto especificados
s.listen(1)  # Habilita la escucha del socket, permite una conexión entrante

while True:  # Bucle principal del servidor
    conn, addr = s.accept()  # Acepta la conexión entrante y obtiene la dirección del cliente
    print("Connected by", addr)  # Muestra la dirección del cliente conectado
    llistaPeers.append(addr)  # Agrega la dirección del cliente a la lista de peers
    command = "ss -ltp"  # Comando para obtener información sobre los sockets de red
    pipeData = Popen(command, shell=True, stdout=PIPE)  # Ejecuta el comando en un proceso secundario
    for line in pipeData.stdout:  # Lee la salida del comando línea por línea
        conn.send(line)  # Envía la línea al cliente conectado
    conn.close()  # Cierra la conexión con el cliente
