#! /usr/bin/python3
#-*- coding: utf-8-*-
#
#@marcramirez1998 ASIX M06 Curs 2023-2024
# 22 server daytime
#------------------------------------------------------
import sys
import socket
from subprocess import Popen, PIPE

HOST = ''  # La dirección IP del servidor (en blanco significa todas las interfaces disponibles)
PORT = 50001  # El puerto en el que el servidor escuchará las conexiones

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # Se crea un socket TCP
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)  # Se configura el socket para reutilizar la dirección
s.bind((HOST, PORT))  # Se enlaza el socket al host y puerto especificados
s.listen(1)  # Se pone el socket en modo de escucha, permitiendo una conexión entrante

conn, addr = s.accept()  # Se acepta la conexión entrante, conn es un nuevo socket que se utiliza para comunicarse con el cliente, addr es la dirección del cliente
print("Conectado por", addr)  # Se imprime la dirección del cliente que se ha conectado

command = ["date"]  # El comando que se ejecutará en el sistema
pipeData = Popen(command, stdout=PIPE)  # Se inicia un proceso para ejecutar el comando y se obtiene una tubería para leer su salida

for line in pipeData.stdout:  # Se itera sobre las líneas de salida del comando
    conn.send(line)  # Se envía cada línea al cliente conectado

conn.close()  # Se cierra la conexión con el cliente
sys.exit(0)  # Se sale del script con un código de salida 0 (indicando que todo está bien)


