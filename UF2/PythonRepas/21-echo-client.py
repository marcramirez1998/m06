#! /usr/bin/python3
#-*- coding: utf-8-*-
#
#@marcramirez1998 ASIX M06 Curs 2023-2024
# 21 echo client
#------------------------------------------------------
import sys,socket
HOST = '' #default es 0.0.0.0 totes les interficies del host
PORT = 50001
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM) #obj tipus socket 
#s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR,1) #liberar puerto
s.connect((HOST, PORT))
s.send(b'Hello, world')
data = s.recv(1024)
s.close()
print('Received', repr(data))
sys.exit(0)


