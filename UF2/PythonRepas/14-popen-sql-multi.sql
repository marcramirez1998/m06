# /user/bin/python3
#-*- coding: utf-8-*-
# @ Abril 2024
# popen_sql.py  -c num_cli - num_cli.....
# ------------------------------
import sys, argparse
from subprocess import Popen, PIPE
parser = argparse.ArgumentParser(description='Consulta SQL interactiva de clients')
parser.add_argument("-d","--database", help="base de dades a usar",\
     default="training")
parser.add_argument('-c',"--client", help='client',type=str,\
     action="append",dest="clieList",required="True")
args = parser.parse_args()
#print(args)
#exit(0)
# -------------------------------------------------------
cmd = "psql -qtA -F',' -h localhost -U postgres "+ args.database
pipeData = Popen(cmd, shell = True, bufsize=0, universal_newlines=True, stdin=PIPE, stdout=PIPE, stderr=PIPE)
for num_clie in args.clieList:
  sqlStatment="select * from clientes where num_clie=%s;" % (num_clie)
  pipeData.stdin.write(sqlStatment+"\n")
  print(pipeData.stdout.readline(), end="")
pipeData.stdin.write("\q\n")
exit(0)
