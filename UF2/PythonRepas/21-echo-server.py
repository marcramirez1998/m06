#! /usr/bin/python3
#-*- coding: utf-8-*-
#
#@marcramirez1998 ASIX M06 Curs 2023-2024
# 21 echo server
#------------------------------------------------------
import sys,socket
HOST = '' #default es 0.0.0.0 totes les interficies del host
PORT = 50001
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM) #obj tipus socket 
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR,1) #liberar puerto
s.bind((HOST,PORT))
s.listen(1)
conn, addr = s.accept() # espera una conexion (se queda parado) genera una tuppla
print("Conn", type(conn), conn) 
#conn  es un socket que representa una conexion establecida
print("Connected by", addr)
while True:
  data = conn.recv(1024)
  if not data: break # no significa no hay datos,significasi han cerrado la conexion
  conn.send(data)
  print(data)
conn.close()
sys.exit(0)

