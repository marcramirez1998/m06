#! /usr/bin/python3
#-*- coding: utf-8-*-
#
#@marcramirez1998 ASIX M06 Curs 2023-2024
# ejemplo fork
#-----------------------------# -------------------------------------
import sys,os
print("Hola, començament del programa principal")
print("PID pare: ", os.getpid())

pid=os.fork()
if pid !=0:
  print("Programa Pare", os.getpid(), pid)
  sys.exit(0)

print("Programa fill", os.getpid(), pid)
os.execv("/usr/bin/python3",["/usr/bin/python3","16-signal.py","70"])

print("Hasta luego lucas!")
sys.exit(0)
