#! /usr/bin/python3
#-*- coding: utf-8-*-
#
#@marcramirez1998 ASIX M06 Curs 2023-2024
# exemple fork
#-----------------------------
import sys,os
print("Hola,començament del programa principal")
print("PID pare:", os.getpid())

pid=os.fork()
if pid !=0:
#    os.wait()
    print("programa pare: ",os.getpid(), pid)
else:
    print("programa fill: ",os.getpid(), pid)
    while True:
        pass
print("hasta luego lucas!")
sys.exit(0)