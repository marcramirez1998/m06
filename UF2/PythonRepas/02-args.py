#! /usr/bin/python3
#-*- coding: utf-8-*-
#
#@marcramirez1998 ASIX M06 Curs 2023-2024
#python documentation: https://docs.python.org/3/library/argparse.html
#argparse tutorial: https://docs.python.org/3/howto/argparse.html
#-----------------------------
import argparse
parser = argparse.ArgumentParser(\
    description="programa d'exemple d'argument",\
    prog="02-arguments.py",\
    epilog="hasta luego lucas")
parser.add_argument ("-n","--nom", type=str,\
    help="nom usuari")
parser.add_argument ("-e","--edat", type=int,\
    dest="userEdat", help="edat a processar",\
        metavar="edat")
args=parser.parse_args()

print(args)
print(args.userEdat,args.nom)
exit(0)