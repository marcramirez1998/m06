import sys
import os
import socket
import argparse
import signal
from subprocess import Popen, PIPE

def mysigterm(signum, frame):
    print("Signal handler called with signal:", signum)
    sys.exit(0)

# Manejo de señales
signal.signal(signal.SIGTERM, mysigterm)

parser = argparse.ArgumentParser(description="Telnet Server")
parser.add_argument("-p", "--port", type=int, default=50001, help="Port number")
args = parser.parse_args()

HOST = ''
PORT = args.port
MYEOF = bytes(chr(4), 'utf-8')

# Creación del socket del servidor y escucha en el puerto especificado
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST, PORT))
s.listen(1)

# Agregar registros para verificar el inicio del servidor
print("Starting Telnet server on port", PORT)
print("Listening for connections...")

while True:
    try:
        # Aceptar conexiones entrantes
        conn, addr = s.accept()
        print("Connected by", addr)  # Mostrar mensaje cuando se conecte un cliente

        while True:
            # Recibir datos del cliente
            data = conn.recv(1024)
            if not data:
                break

            # Ejecutar el comando recibido y enviar la salida al cliente
            pipeData = Popen(data, stdout=PIPE, shell=True)
            for line in pipeData.stdout:
                conn.sendall(line)
            conn.send(MYEOF)

        conn.close()
    except Exception as e:
        print("Error:", e)
