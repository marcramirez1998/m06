#! /usr/bin/python3
#-*- coding: utf-8-*-
#
#@marcramirez1998 ASIX M06 Curs 2023-2024
# ejemplo fork
#-----------------------------
import sys
import os
import signal

def myusr1(signum, frame):
    print("Signal handler called with signal:", signum)
    print("Hola myusr1:")

def myusr2(signum, frame):
    print("Signal handler called with signal:", signum)
    print("Adiós myusr2:")

print("Hola, comienzo del programa principal")
print("PID padre:", os.getpid())
pid = os.fork()
if pid != 0:
    #os.wait()  
    print("Programa padre:", os.getpid(), pid)
    print("Llança el proces fill servidor")
    sys.exit(0)

    print("Programa fill:", os.getpid(), pid)
    signal.signal(signal.SIGUSR1, myusr1)  # 10
    signal.signal(signal.SIGUSR2, myusr2)  # 12
    while True:
        pass  
# Aixo no s'executara mai
print("¡Hasta luego, Lucas!")
sys.exit(0)
