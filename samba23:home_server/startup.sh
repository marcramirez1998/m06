#! /bin/bash
# @sarayc ASIX-M06
# -------------------------------------

# crea users unix
for user in unix01 unix02 unix03 unix04 unix05
do
  useradd -m -s /bin/bash $user
  echo -e "$user\n$user" | passwd $user
done
# dimonis
/usr/sbin/nslcd
/usr/sbin/nscd

# Share pub
mkdir -p /var/lib/samba/public
chmod 777 /var/lib/samba/public
cp /opt/docker/* /var/lib/samba/public/.

# Share priv
mkdir -p /var/lib/samba/privat
cp /etc/os-release /var/lib/samba/privat/.

# cp conf
cp /opt/docker/smb.conf /etc/samba/smb.conf

# crea users samba
for user in lila roc patipla pla
do
  useradd -m -s /bin/bash $user
  echo -e "$user\n$user" | smbpasswd -a $user
done

# perms shells
chmod +x /opt/docker/fitxer.sh
bash /opt/docker/fitxer.sh

# dimonis
/usr/sbin/smbd 
/usr/sbin/nmbd -F

pdbedit -L
