# LDAP EDITAT


## Executar container:

```
docker build -t marcrami1998/ldap23:editat .
```

```
docker run --rm --name ldap-editat -p 389:389 -d marcrami1998/ldap23:editat
```

## PAS 1:

### Crear Dockerfile:

```
# ldapserver 2023
FROM debian:latest
LABEL version="1.0"
LABEL author="@marcrami1998"
LABEL subject="ldapserver 2023"
RUN apt-get update
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get install -y procps iproute2 tree nmap vim ldap-utils slapd
RUN mkdir /opt/docker
COPY * /opt/docker/
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
CMD /opt/docker/startup.sh
EXPOSE 389
```

## PAS 2:

### Crear el fitxer startup.sh

```
#!/bin/bash

echo "Configurant el servidor ldap..."

rm -rf /etc/ldap/slapd.d/*
rm -rf /var/lib/ldap/*
slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
slapadd  -F /etc/ldap/slapd.d -l /opt/docker/edt-org.ldif
chown -R openldap:openldap /etc/ldap/slapd.d /var/lib/ldap
slapcat
/usr/sbin/slapd -d0
```

## PAS 3:

### Fitxers:

Copiem el fitxer slapd.conf i edt-org.ldif, dins del fitxer edt-org.ldif afegim usuaris amb format uid, una nova OU i 2 usuaris nous.

#### Usuaris nous:

```
# Nou user01

dn: uid=user01,ou=grupclase,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: user1
cn: user01
sn: user01
homephone: 555-222-2220
mail: user01@edt.org
description: exemple user 01
ou: grupclase
uid: user01
uidNumber: 5000
gidNumber: 100
homeDirectory: /tmp/home/user01
userPassword: user01



# Nou usuari02

dn: uid=user02,ou=grupclase,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: user2
cn: user02
sn: user02
homephone: 555-222-2220
mail: user02@edt.org
description: exemple user 02
ou: grupclase
uid: user02
uidNumber: 5001
gidNumber: 101
homeDirectory: /tmp/home/user02
userPassword: user02



# Nou OU

dn: ou=nougrup,dc=edt,dc=org
ou: nougrup
description: nougrup
objectclass: organizationalunit

```


## PAS 4:

### Encriptar la password:

```
slappasswd -s secret
```

```
#contra secret

rootpw {SSHA}DIKADJbO1Wy4e1htZw7XTbiKsd1ofpNc
```
Esborrem la password secret i posem la encriptada dins del fitxer slapd.conf


## Fitxer per canviar la password en calent amb un fitxer:

```
vim mod1.ldif
```

```
dn: olcDatabase={1}mdb,cn=config
changetype: modify
replace: olcRootPW
olcRootPW: patata
-
delete: olcAccess
-
add: olcAccess
olcAccess: to * by * write
```

### Comprovació:

```
ldapmodify -x -D 'cn=Sysadmin,cn=config' -w syskey -f mod1.ldif
```

```
ldapsearch -x -LLL -D 'cn=Sysadmin,cn=config' -w syskey -b 'olcDatabase={1}mdb,cn=config' olcRootPW olcAccess
```

