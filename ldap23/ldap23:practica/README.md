# LDAP PRACTICA 2023


## Crear el ldap:

```
docker build -t marcramirez1998/ldap23:practica .
```
## Executar compose:

```
docker-compose up -d
```

## Entra desde internet a phpldapadmin

```
http://localhost/phpldapadmin/

```


## Crear un schema que contingui:

### STRUCTURAL

```
objectClass (1.1.2.1.1.1 NAME 'x-judoka'
  DESC 'judoka'
  SUP TOP
  STRUCTURAL
  MUST ( x-nom $ x-club )
  MAY ( x-imatge $ x-documentacio ))

```

### AUXILIARY

```
objectClass (1.1.2.2.1.1 NAME 'x-pais'
  DESC 'Pais del judoka'
  SUP TOP
  AUXILIARY
  MUST ( x-actiu $ x-lloc-neixement $ x-ciutat ))

```

## Atributs del objecte STRUCTURAL


```
attributetype (1.1.2.1.1.1 NAME 'x-nom'
  DESC 'Nom del judoka'
  EQUALITY caseIgnoreMatch
  SUBSTR caseIgnoreSubstringsMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.15
  SINGLE-VALUE)

```

### A quin club pertany

```

attributetype (1.1.2.1.1.2 NAME 'x-club'
  DESC 'A quin club pertany'
  EQUALITY caseIgnoreMatch
  SUBSTR caseIgnoreSubstringsMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.15
  SINGLE-VALUE)

```

### Documentacio (pdf)

```
attributetype (1.1.2.1.1.4 NAME 'x-documentacio'
  DESC 'documentacio del judoka'
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.5)

```

### Imatge

```
attributetype (1.1.2.1.1.3 NAME 'x-imatge'
  DESC 'Imatge del judoka'
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.28)

```

## Atributs del objecte AUXILIARY

### Si esta actiu

```
attributetype (1.1.2.2.1.1 NAME 'x-actiu'
  DESC 'Esta actiu'
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.7
  SINGLE-VALUE)

```

### Lloc de neixement

```

attributetype (1.1.2.2.1.2 NAME 'x-lloc-neixement'
  DESC 'Lloc de neixement'
  EQUALITY caseIgnoreMatch
  SUBSTR caseIgnoreSubstringsMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.15
  SINGLE-VALUE)

```

### Ciutat del judoka

```
attributetype (1.1.2.2.1.3 NAME 'x-ciutat'
  DESC 'Ciutat del judoka'
  EQUALITY caseIgnoreMatch
  SUBSTR caseIgnoreSubstringsMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.15
  SINGLE-VALUE)

```

## Crear entitats

### Entitat 1:

```

dn: x-nom=orujov,ou=practica,dc=edt,dc=org
objectClass: x-judoka
objectClass: x-pais
x-nom: orujov
x-club: rei
x-imatge:< file:/opt/docker/orujov.jpeg
x-documentacio:< file:/opt/docker/orujov.pdf
x-actiu: True
x-lloc-neixement: Azerbaijan
x-ciutat: Baku

```

### Entitat 2:

```
dn: x-nom=ono,ou=practica,dc=edt,dc=org
objectClass: x-judoka
objectClass: x-pais
x-nom: ono
x-club: bushi
x-imatge:< file:/opt/docker/ono.jpeg
x-documentacio:< file:/opt/docker/ono.pdf
x-actiu: False
x-lloc-neixement: Japo
x-ciutat: Tokio

```

### Entitat 3

```

dn: x-nom=vieru,ou=practica,dc=edt,dc=org
objectClass: x-judoka
objectClass: x-pais
x-nom: vieru
x-club: psg
x-imatge:< file:/opt/docker/vieru.jpeg
x-documentacio:< file:/opt/docker/vieru.pdf
x-actiu: False
x-lloc-neixement: Moldavia
x-ciutat: Chisinau

```

