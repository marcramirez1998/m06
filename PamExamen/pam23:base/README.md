## Crear la imatge:

```
docker build -t marco3008/pam23:base .
```

## Executar el container pam:

```
docker run --rm --name pam.edt.org -h pam.edt.org --net 2hisx --privileged -it marco3008/pam23:base
```

