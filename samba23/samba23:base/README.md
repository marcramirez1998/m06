﻿**Samba**

**Win**

● **Recursos** PROTOCOL: (SMB antic i el CIFS es el nou)

- Carpetas
- Impressores

**Modelo 1**

![](imagenes/Aspose.Words.2c3e73e3-1b5f-4943-b5b0-235c9be25b3b.001.png)

**Modelo 2** hace que el servidor linux que haga pasar como windows y los clientes windows se piensan que es windows.

![](imagenes/Aspose.Words.2c3e73e3-1b5f-4943-b5b0-235c9be25b3b.002.png)

**Domini/Organitzacio**

**Active directory abans era PDC i BDC**

![](imagenes/Aspose.Words.2c3e73e3-1b5f-4943-b5b0-235c9be25b3b.003.png)

**a183077mr@i12:/var/tmp/m06/samba23/samba23:base$** vim Dockerfile

![](imagenes/Aspose.Words.2c3e73e3-1b5f-4943-b5b0-235c9be25b3b.004.png)

**a183077mr@i12:/var/tmp/m06/samba23/samba23:base$** vim startup.sh

![](imagenes/Aspose.Words.2c3e73e3-1b5f-4943-b5b0-235c9be25b3b.005.png)

docker build -t marcramirez1998/samba23:base .

![](imagenes/Aspose.Words.2c3e73e3-1b5f-4943-b5b0-235c9be25b3b.006.jpeg)

docker run --rm --name samba.edt.org -h samba.edt.org --net 2hisx -it marcramirez1998/samba23:base /bin/bash

docker run --rm --name samba.edt.org -h samba.edt.org --net 2hisx --privileged -it marcramirez1998/samba23:base /bin/bash

![](imagenes/Aspose.Words.2c3e73e3-1b5f-4943-b5b0-235c9be25b3b.007.png)

**root@samba:/opt/docker#** tree /var/lib/s

![](imagenes/Aspose.Words.2c3e73e3-1b5f-4943-b5b0-235c9be25b3b.008.png)

**root@samba:/opt/docker#** cp /etc/samba/smb.conf . **root@samba:/opt/docker#** vim /etc/samba/smb.conf

![](imagenes/Aspose.Words.2c3e73e3-1b5f-4943-b5b0-235c9be25b3b.009.png)

**Windows treballa amb** WORKGROUP guest ok es el mateix que dir public yes vim /etc/samba/smb.conf

**Compartir recurs** /usr/share/doc

![](imagenes/Aspose.Words.2c3e73e3-1b5f-4943-b5b0-235c9be25b3b.010.png)

**Compartir recurs** /usr/share/man

![](imagenes/Aspose.Words.2c3e73e3-1b5f-4943-b5b0-235c9be25b3b.011.png)

**Verificar funcionamiento** recursos compartidos

![](imagenes/Aspose.Words.2c3e73e3-1b5f-4943-b5b0-235c9be25b3b.012.jpeg)

Samba te dos dimonis

/usr/sbin/smbd.service: dimoni de comparticio d’impresores /usr/sbin/nmbd.service

Encender demonios

**root@samba:/opt/docker#** /usr/sbin/smbd **root@samba:/opt/docker#** /usr/sbin/nmbd

![](imagenes/Aspose.Words.2c3e73e3-1b5f-4943-b5b0-235c9be25b3b.013.png)

No hace falta hacer un sistemctl restart en samba para aplicar cambios Que puertos usa samba: 139 i 445 amb protocol tcp.

![](imagenes/Aspose.Words.2c3e73e3-1b5f-4943-b5b0-235c9be25b3b.014.png)

![](imagenes/Aspose.Words.2c3e73e3-1b5f-4943-b5b0-235c9be25b3b.015.png)

**Arrancar cliente samba:**

a183077mr@i12:/var/tmp/m06/samba23/samba23:base$ docker run --name client -h client --net 2hisx -it marcramirez1998/samba23:base /bin/bash

**Listar recursos compartidos:** smbclient -L //samba.edt.org

![](imagenes/Aspose.Words.2c3e73e3-1b5f-4943-b5b0-235c9be25b3b.016.png)

Entrar en recurso documentation

![](imagenes/Aspose.Words.2c3e73e3-1b5f-4943-b5b0-235c9be25b3b.017.png)

**bajar archivo** get nombrearchivo

**Subir archivo** put nombrearchivo newnombre

**Directives a practicar**

path = /dir1/dir2/share

comment = share description

volume = share name

browseable = yes/no

max connections = #

public = yes/no

guest ok = yes/no

guest account = unix-useraccount

guest only = yes/no

valid users = user1 user2 @group1 @group2 ... invalid users = user1 user2 @group1 @group2 ... auto services = user1 user2 @group1 @group2 ... admin users = user1 user2 @group1 @group2 ... writable = yes/no

read only = yes/no

write list = user1 user2 @group1 @group2 ... read list = user1 user2 @group1 @group2 ... create mode = 0660

directory mode = 0770

**path**: Especifica la ruta del directorio que se compartirá.

**comment**: Proporciona una descripción de la compartición que se mostrará a los usuarios.

**volume**: Define el nombre de la compartición.

**browseable**: Indica si la compartición es visible para los clientes que navegan por la red. Puede ser configurado como "yes" para que sea visible y "no" para ocultarlo.

**max connections**: Limita el número máximo de conexiones simultáneas permitidas a la compartición.

**public**: Indica si la compartición es pública y accesible para todos los usuarios. Puede ser configurado como "yes" para permitir el acceso público y "no" para requerir autenticación.

**guest ok**: Permite el acceso de usuarios invitados a la compartición. Puede ser configurado como "yes" para permitir el acceso de usuarios invitados y "no" para requerir autenticación.

**guest account**: Especifica la cuenta de usuario de Unix que se utilizará para los usuarios invitados.

**guest only**: Indica si solo se permite el acceso a usuarios invitados. Puede ser configurado como "yes" para restringir el acceso solo a usuarios invitados y "no" para permitir el acceso a cualquier usuario.

**valid users**: Solo usuarios válidos que tienen acceso a la compartición. **invalid users**: usuarios no válidos, que no tienen acceso a la compartición.

**auto services**: Especifica los usuarios y grupos que tendrán acceso automático a los servicios de esta compartición.

**admin users**: Enumera los usuarios y grupos de usuarios que tendrán privilegios de administrador sobre la compartición.

**writable**: Indica si la compartición se puede escribir. Puede ser configurado como "yes" para permitir escritura y "no" para solo permitir lectura.

**read only**: Indica si la compartición es de solo lectura. Puede ser configurado como "yes" para restringir la compartición a solo lectura y "no" para permitir escritura.

**write list**: autorizados para escribir en la compartición. **read list**: autorizados para leer la compartición.

**create mode**: Establece los permisos de archivo para los archivos creados en la compartición.

**directory mode**: Establece los permisos de directorio para los directorios creados en la compartición.

**mapping** jo soc marc y al accedir al sistema de fitches soc nobody.

**NEW PRACTICA**

docker run --rm --name samba.edt.org -h samba.edt.org --net 2hisx --privileged -it marcramirez1998/samba23:base /bin/bash

bash startup.sh

root@samba:/opt/docker# /usr/sbin/smbd

root@samba:/opt/docker# /usr/sbin/nmbd

docker run --rm --name client.edt.org -h client.edt.org --net 2hisx --privileged -it marcramirez1998/samba23:base /bin/bash

**listar los recursos compartidos disponibles del server samba.edt.org.**

smbclient -L //samba.edt.org

![](imagenes/Aspose.Words.2c3e73e3-1b5f-4943-b5b0-235c9be25b3b.018.jpeg)

Entrar en el recurso compartido y listar smbclient //samba.edt.org/public

![](imagenes/Aspose.Words.2c3e73e3-1b5f-4943-b5b0-235c9be25b3b.019.png)

Descargar al /tmp del docker con el nombre kak1

get startup.sh /tmp/kak1

![](imagenes/Aspose.Words.2c3e73e3-1b5f-4943-b5b0-235c9be25b3b.020.png)

![](imagenes/Aspose.Words.2c3e73e3-1b5f-4943-b5b0-235c9be25b3b.021.png)

Subir el /etc/os-release de mi maquina dentro del docker con otro nombre put /etc/os-release file1

![](imagenes/Aspose.Words.2c3e73e3-1b5f-4943-b5b0-235c9be25b3b.022.png)

**Descargar un archivo desde un recurso compartido en un servidor Samba y guardarlo en el sistema local.**

smbget smb://samba.edt.org/public/smb.conf

![](imagenes/Aspose.Words.2c3e73e3-1b5f-4943-b5b0-235c9be25b3b.023.png)

Montar un recurs com a tipus cifs com a usuari invitat del recurs compartit public a mnt

**root@client:/opt/docker#** mount -t cifs -o guest //samba.edt.org/public /mnt **root@client:/opt/docker#** mount -t cifs

![](imagenes/Aspose.Words.2c3e73e3-1b5f-4943-b5b0-235c9be25b3b.024.png)

**Desmontar mnt:** umount /mnt

![](imagenes/Aspose.Words.2c3e73e3-1b5f-4943-b5b0-235c9be25b3b.025.png)

![](imagenes/Aspose.Words.2c3e73e3-1b5f-4943-b5b0-235c9be25b3b.026.png)

![](imagenes/Aspose.Words.2c3e73e3-1b5f-4943-b5b0-235c9be25b3b.027.png)

![](imagenes/Aspose.Words.2c3e73e3-1b5f-4943-b5b0-235c9be25b3b.028.jpeg)

**smbget i smbclient** saben resoldre el nom samba con a samba.edt.org **nmap** no sap tende de fer samba.edt.or

**samba es el seu nom windows** Qui resol es windows

**Para resolver nombre es** nmblookup samba

![](imagenes/Aspose.Words.2c3e73e3-1b5f-4943-b5b0-235c9be25b3b.029.png)

**Ver status resolucion de nombre** nmblookup -S samba

![](imagenes/Aspose.Words.2c3e73e3-1b5f-4943-b5b0-235c9be25b3b.030.png)

**MSBROWSE EL PUTO AMO**

Samba no crea el seus propis usuaris els usuaris estan recolsats en usuaris unix. samba te el seu propi fitxer de usuaris pero (sols te passwd el fitcher)

/var/lib/samba/account\_policy.tdb /var/lib/samba/group\_mapping.tdb

**Listar si se han creado los usuarios:** pdbedit -L **Ver informacion de lila:** pdbedit -v lila

![](imagenes/Aspose.Words.2c3e73e3-1b5f-4943-b5b0-235c9be25b3b.031.jpeg)

ENtrar con usuario

![](imagenes/Aspose.Words.2c3e73e3-1b5f-4943-b5b0-235c9be25b3b.032.png)

ENtrar con usuario y password en lina de comando

![](imagenes/Aspose.Words.2c3e73e3-1b5f-4943-b5b0-235c9be25b3b.033.png)

Subir el /etc/services de mi pc dentro de public como nombre de servei

![](imagenes/Aspose.Words.2c3e73e3-1b5f-4943-b5b0-235c9be25b3b.034.jpeg)

Entrar en el directorio del usuario y comprobar.

![](imagenes/Aspose.Words.2c3e73e3-1b5f-4943-b5b0-235c9be25b3b.035.png)

**Samba práctica**

cd /var/tmp/m06/samba23/samba23\:base/

docker run --rm --name samba.edt.org -h samba.edt.org --net 2hisx --privileged -it marcramirez1998/samba23:base /bin/bash

bash startup

![](imagenes/Aspose.Words.2c3e73e3-1b5f-4943-b5b0-235c9be25b3b.036.jpeg)

a183077mr@i12:/var/tmp/m06/samba23/samba23:base$ docker run --rm --name samba.edt.org -h samba.edt.org --net 2hisx -d marcramirez1998/samba23:base

a183077mr@i12:/var/tmp/m06/samba23/samba23:base$ docker run --rm --name client -h client --net 2hisx -d marcramirez1998/samba23:base

docker exec -it samba.edt.org /bin/bash

Exemple 1: Usuari guest fa idmap a nobody root@samba:/opt/docker# vim smb.conf

![](imagenes/Aspose.Words.2c3e73e3-1b5f-4943-b5b0-235c9be25b3b.037.png)

root@client:/opt/docker# smbclient -N //samba/public

smb: \> ls

smb: \> get smb.conf

getting file \smb.conf of size 1411 as smb.conf (459.3 KiloBytes/sec) (average 459.3 KiloBytes/sec)

root@samba:/opt/docker# touch hola.txt root@samba:/opt/docker# bash startup.sh smb: \> get hola.txt

smb: \> put hola.txt put2.txt

![](imagenes/Aspose.Words.2c3e73e3-1b5f-4943-b5b0-235c9be25b3b.038.png)

![](imagenes/Aspose.Words.2c3e73e3-1b5f-4943-b5b0-235c9be25b3b.039.jpeg)

root@samba:/opt/docker# ls /var/lib/samba/public/

![](imagenes/Aspose.Words.2c3e73e3-1b5f-4943-b5b0-235c9be25b3b.040.png)

Exemple 2: Només usuari guest

vim /etc/samba/smb.conf

![](imagenes/Aspose.Words.2c3e73e3-1b5f-4943-b5b0-235c9be25b3b.041.png)

**En el cliente y servidor añadir users**

for user in lila roc patipla pla

do

useradd -m -s /bin/bash $user

echo -e "$user\n$user" | smbpasswd -a $user done

pdbedit -L

root@client:/opt/docker# smbclient -U lila //samba/public

Password for [WORKGROUP\lila]:

Try "help" to get a list of possible commands.

smb: \> get hola.txt

getting file \hola.txt of size 0 as hola.txt (0.0 KiloBytes/sec) (average 0.0 KiloBytes/sec)

smb: \> put hola.txt holalila.txt

putting file hola.txt as \holalila.txt (0.0 kb/s) (average 0.0 kb/s)

![](imagenes/Aspose.Words.2c3e73e3-1b5f-4943-b5b0-235c9be25b3b.042.png)

root@samba:/opt/docker# ls /var/lib/samba/public/

![](imagenes/Aspose.Words.2c3e73e3-1b5f-4943-b5b0-235c9be25b3b.043.png)

Exemple 3: Usuari només guest amb idmap a un compte unix (deprecated guest account?)

![](imagenes/Aspose.Words.2c3e73e3-1b5f-4943-b5b0-235c9be25b3b.044.png)

![](imagenes/Aspose.Words.2c3e73e3-1b5f-4943-b5b0-235c9be25b3b.045.png)

![](imagenes/Aspose.Words.2c3e73e3-1b5f-4943-b5b0-235c9be25b3b.046.png)

Resultat

![](imagenes/Aspose.Words.2c3e73e3-1b5f-4943-b5b0-235c9be25b3b.047.png)

Exemple 4: Usuari identificat root@samba:/opt/docker# vim /etc/samba/smb.conf

![](imagenes/Aspose.Words.2c3e73e3-1b5f-4943-b5b0-235c9be25b3b.048.png)

![](imagenes/Aspose.Words.2c3e73e3-1b5f-4943-b5b0-235c9be25b3b.049.png)

![](imagenes/Aspose.Words.2c3e73e3-1b5f-4943-b5b0-235c9be25b3b.050.png)

Exemple 5: Valid users

![](imagenes/Aspose.Words.2c3e73e3-1b5f-4943-b5b0-235c9be25b3b.051.png)

![](imagenes/Aspose.Words.2c3e73e3-1b5f-4943-b5b0-235c9be25b3b.052.png)

Exemple 6: Invalid users

![](imagenes/Aspose.Words.2c3e73e3-1b5f-4943-b5b0-235c9be25b3b.053.png)

![](imagenes/Aspose.Words.2c3e73e3-1b5f-4943-b5b0-235c9be25b3b.054.png)

Exemple 7: Admin users

![](imagenes/Aspose.Words.2c3e73e3-1b5f-4943-b5b0-235c9be25b3b.055.png)

![](imagenes/Aspose.Words.2c3e73e3-1b5f-4943-b5b0-235c9be25b3b.056.png)

![](imagenes/Aspose.Words.2c3e73e3-1b5f-4943-b5b0-235c9be25b3b.057.png)

Exemple 8: Recurs de només lectura

read only = yes

writable = no

Els recursos es poden configurar per ser de només lectura (són equivalents).

Exemple 9: Recurs de lectura/escriptura

read only = no

writable = yes

Recursos configurats de lectura/escriptura. Són equivalents

Exemple 10: Llista d’usuaris autoritzats de lectura

Indica la llista d’usuaris que només poden llegir. Atenció, aquesta directiva s’indica en recursos que són de lectura/escriptura i serveix per restringir aquests usuaris atorgant-los només el dret de lectura (i privant-los del de escriptura

![](imagenes/Aspose.Words.2c3e73e3-1b5f-4943-b5b0-235c9be25b3b.058.png)

Exemple 11: Llista d’usuaris autoritzats per a escriptura

Indica la llista d’usuaris amb dret d’escriptura al recurs. S’utilitza en recursos que són read only per a tots els usuaris però es permet als usuaris indicats a la llista el dret de escriptura.

![](imagenes/Aspose.Words.2c3e73e3-1b5f-4943-b5b0-235c9be25b3b.059.png)

Exemple 12: Modes de directori i fitxer

![](imagenes/Aspose.Words.2c3e73e3-1b5f-4943-b5b0-235c9be25b3b.060.png)
