#!/bin/bash
# @marcramirez1998
# ---------------------
#share public
mkdir -p /var/lib/samba/public
chmod 777 /var/lib/samba/public
cp /opt/docker/* /var/lib/samba/public/.

#share privat
mkdir -p /var/lib/samba/privat
cp /etc/os-release /var/lib/samba/privat

#copiar configuracio
cp /opt/docker/smb.conf /etc/samba/smb.conf


# Creació usuaris unix/samba
for user in lila roc patipla pla smbunix01 smbunix02 smbunix03 smbunix04 smbunix05
do	
  useradd -m -s /bin/bash $user
  echo -e "$user\n$user" | smbpasswd -a $user
done
pdbedit -L

/usr/sbin/smbd && echo "smb Ok"
/usr/sbin/nmbd -F && echo "nmb  Ok"
