#! /usr/bin/python
#-*- coding: utf-8 -*-
""" 
# -----------------------------------------------
@edt Desembre 2014
ASIX-M06
telnet server program
# -----------------------------------------------
"""
import socket, sys
from  subprocess import Popen, PIPE
HOST = ''
PORT = 50007

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((HOST, PORT))
s.listen(1)
conn, addr = s.accept()
print 'Connected by', addr

while True:
    command = conn.recv(1024)
    if not command: 
        sys.stderr.write("Connection closed by client!")
        break
    sys.stderr.write(command+'\n')
    popenData = Popen(command, shell=True, stdout=PIPE, stdin=PIPE)
    for linia in popenData.stdout:
        conn.send(linia)
    conn.send(chr(4))
conn.close()
s.close()
sys.exit(0)
    


