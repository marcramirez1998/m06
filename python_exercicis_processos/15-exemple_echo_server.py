#! /usr/bin/python
#-*- coding: utf-8 -*-
# @ edt Desembre 2014
# ASIX-M06
# echo server program
# -----------------------------------------------
import socket
HOST = ''
PORT = 50007 

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((HOST, PORT))
s.listen(1)
conn, addr = s.accept()
print 'Connected by', addr
print 'Connection ', conn

while True:
    data = conn.recv(1024)
    if not data: break
    print data
    conn.sendall(data)
conn.close()

