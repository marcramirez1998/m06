#! /usr/bin/python
# Llistar psql clientes via popen
# -----------------------------------------------
# @edt Desembre 2014
# ASX M06-ASO Scripts
# -----------------------------------------------
import sys
from subprocess import Popen, PIPE
cmd="psql -d training -c \"select * from cliente;\" -h 192.168.2.37"
pipeData=Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=PIPE)

for line in pipeData.stdout:
    print line[:-1]
sys.exit(0)
