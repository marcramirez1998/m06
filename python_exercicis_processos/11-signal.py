#! /usr/bin/python
import signal, sys
def terminate(signum, frame):
    print 'Signal handler called with signal', signum
    print "closing program..."
    sys.exit(0)
def happyDay(signum, frame):
    print "Congratulations!", signum, "signal selected"
    print "Have a nice day!"
def sigReload(signum, frame):
    print "MATRIX reloaded!"
    print "where is sion?"

# Set the signal handler and a 5-second alarm
signal.signal(signal.SIGALRM, terminate)  #14
signal.signal(signal.SIGUSR1, terminate)  #10
signal.signal(signal.SIGUSR2, happyDay)   #12
signal.signal(signal.SIGHUP, sigReload)   # 1
signal.signal(signal.SIGINT, signal.SIG_IGN) # ignore ctrl+c #2
signal.alarm(60)


while True:
    pass
