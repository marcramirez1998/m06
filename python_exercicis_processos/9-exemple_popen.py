#!/usr/bin/python
#-*- coding: utf-8-*-
'''
# llista un directori 
# ­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­ 
# Escola del treball de Barcelona 
# ASIX Hisi2 M06­ASO UF2NF1­Scripts 
# Curs 2014­2015  Desembre 2014 
# ########################################## 
'''
import sys
from optparse import OptionParser
from subprocess import Popen,   PIPE
usage='%prog [options] [arg]\ntype %prog -h or %prog --help per a mes informacio'
parser = OptionParser(usage)
parser.add_option("-d", "--directory", dest="ruta", help="Introducir la ruta")
(options, args) = parser.parse_args()
# Executa el pipe/subprocess
command = ["ls", options.ruta]
pipeData = Popen(command, stdout=PIPE, stderr=PIPE)
print pipeData

sys.exit(0)
