#! /usr/bin/python
# prog cliecod...
# -----------------------------------------------
# @edt Desembre 2014
# ASX M06-ASO Scripts
# -----------------------------------------------
import sys, argparse
from subprocess import Popen, PIPE
parser = argparse.ArgumentParser(description='llistar dades de cada client')
parser.add_argument(dest='clientList', help='num clie a la BD (cliecod)', nargs='+')
args = parser.parse_args()
#clientList=['2111','2102','2103','2123']

cmd="psql -qtA -d training -h 192.168.2.37"
pipeData=Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=PIPE)

for cliecod in args.clientList:
    sql="select * from cliente where cliecod=%s;\n" % (cliecod)
    pipeData.stdin.write(sql)
    print pipeData.stdout.readline()[:-1]
sys.exit(0)

