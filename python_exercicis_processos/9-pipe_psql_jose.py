#!/usr/bin/python
#-*- coding: utf-8-*-
# Jose Luis Grande Castro
# isx41011398
# @edt exemples - objectes
#

import sys
import argparse
from subprocess import Popen,   PIPE
box = argparse.ArgumentParser() # Constructor d'objecte
box.add_argument(dest='sentencia', help='Insereix la sentencia sql per fer la consulta.')
# posar tots els usuaris en una llista on cadea llista és un objecte usuari
args = box.parse_args()
sentencia=args.sentencia


cmd="psql -qtA -d training -c \"%s;\" -h 192.168.2.59"%(sentencia)
pipeData=Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=PIPE)

for line in pipeData.stdout:
    print line[:-1]
sys.exit(0)
